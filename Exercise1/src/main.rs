// You should implement the following function:

fn sum_of_multiples(mut number: i32, multiple1: i32, multiple2: i32) -> i32 
{
    let mut sum = 0;
    let mut x = 0;
    let mut num1 = 0;
    let mut num2 = 0;
    while num1 < number || num2 < number {
        x += 1;
        num1 = multiple1*x;
        num2 = multiple2*x;
        if num1 < number {
            sum += num1;
        }
        if num2 < number && num2%multiple1 != 0 {
            sum += num2;
        }
    }
    return sum
        
}

fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
}
